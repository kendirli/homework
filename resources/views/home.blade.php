@extends('layouts.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-6 col-6">
                        <!-- small box -->
                        <table class="table table-bordered table-striped">
                            <tr class="bg-blue">
                                <td>#</td>
                                <td>Başlık</td>
                                <td>İçerik</td>
                                <td>Oluşturulma</td>
                            </tr>
                            <?php $i=1; ?>
                            @foreach($lastTens as $last)
                                <tr>
                                    <td>{{($i)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($last->title,35)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($last->content,35)}}</td>
                                    <td>{{$last->created_at->locale('tr')}}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </table>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-6">
                        <!-- small box -->
                        <table class="table table-bordered table-striped">
                            <tr class="bg-success">
                                <td>#</td>
                                <td>Başlık</td>
                                <td>İçerik</td>
                                <td>Oluşturulma</td>
                            </tr>
                            <?php $i=1; ?>
                            @foreach($openHighs as $openHigh)
                                <tr>
                                    <td>{{($i)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($openHigh->title,35)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($openHigh->content,35)}}</td>
                                    <td>{{$openHigh->created_at->locale('tr')}}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </table>
                    </div>
                    <!-- ./col -->
                </div>
                <div class="row">
                    <div class="col-lg-6 col-6">
                        <!-- small box -->
                        <table class="table table-bordered table-striped">
                            <tr class="bg-cyan">
                                <td>#</td>
                                <td>Başlık</td>
                                <td>İçerik</td>
                                <td>Oluşturulma</td>
                            </tr>
                            <?php $i=1; ?>
                            @foreach($delayeds as $delayed)
                                <tr>
                                    <td>{{($i)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($delayed->title,35)}}</td>
                                    <td>{{\Illuminate\Support\Str::limit($delayed->content,35)}}</td>
                                    <td>{{$delayed->created_at->locale('tr')}}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </table>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-6">
                        <!-- small box -->
                        <table class="table table-bordered table-striped">
                            <tr class="bg-danger">
                                <td>#</td>
                                <td>Ülke</td>
                                <td>Açık Ticket</td>
                            </tr>
                            <?php $i=1; ?>
                            @foreach($countrys as $country)
                                <tr>
                                    <td>{{($i)}}</td>
                                    <td>{{$country->ulke}}</td>
                                    <td>{{$country->total}}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </table>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('java-script')
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('back/dist/js/pages/dashboard.js')}}"></script>
@endsection

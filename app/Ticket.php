<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;
    protected $table = 'tickets';
    protected $dates = [
        'created_at','updated_at','deleted_at'
    ];

    protected $fillable = [
        'user_id','country_id','title','content','status','priority'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware'=>['auth']],function (){
    Route::get('/home', 'HomeController@index');

    Route::get('/tickets', 'TicketController@index');
    Route::get('/tickets/create', 'TicketController@create');
    Route::post('/tickets/save', 'TicketController@store');
    Route::get('/tickets/{status}', 'TicketController@indexChangeStatus');
    Route::get('/tickets/close/{id}', 'TicketController@closeTicket');
    Route::get('/tickets/edit/{id}', 'TicketController@edit');
    Route::post('/tickets/update/{id}', 'TicketController@update');
    Route::get('/tickets/delete/{id}', 'TicketController@destroy');
});

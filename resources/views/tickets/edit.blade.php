@extends('layouts.master')
@section('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('back/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('back/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Yeni Ticket</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="post" action="/tickets/update/{{$ticket->id}}">
                                @csrf
                                <input type="hidden" name="status" value="{{$ticket->status}}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Başlık</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                               placeholder="Başlık" value="{{$ticket->title}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Ülke</label>
                                        <select class="form-control select2" name="country_id" style="width: 100%;">
                                            <option selected>Ülke Seç</option>
                                            @foreach($countrys as $country)
                                                @if($country->id == $ticket->country_id)
                                                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                                                @else
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Önem Seviyesi</label>
                                        <select class="form-control select2" name="priority" style="width: 100%;">
                                            <option value="1" @if($ticket->priority == 1) selected @endif>Yüksek</option>
                                            <option value="2" @if($ticket->priority == 2) selected @endif>Orta</option>
                                            <option value="3" @if($ticket->priority == 3) selected @endif>Düşük</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="content">İçerik</label>
                                        <textarea name="content" class="form-control" id="content" placeholder="İçerik"
                                                  rows="8">{{$ticket->content}}</textarea>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Güncelle</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!--/.col (left) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
    </div>
@endsection
@section('java-script')
    <!-- Select2 -->
    <script src="{{asset('back/plugins/select2/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        //Initialize Select2 Elements
        $('.select2').select2()
    </script>
@endsection
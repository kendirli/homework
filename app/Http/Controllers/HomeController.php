<?php

namespace App\Http\Controllers;

use App\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lastTens = Ticket::all()->sortByDesc('id')->take(10);
        $openHighs = Ticket::all()->where('status',1)
            ->where('priority',1)
            ->sortByDesc('id')->take(10);
        $time = Carbon::now()->subWeek(1);

        $delayeds = Ticket::all()
            ->where('created_at','<',$time)
            ->sortBy('created_at')->take(10);
        $countrys = DB::select("SELECT t.country_id,c.name as ulke,COUNT(t.country_id) AS total 
                                FROM tickets as t
                                INNER JOIN country as c ON t.country_id = c.id
                                WHERE t.status = 1 GROUP BY t.country_id
                                ORDER BY COUNT(t.country_id) DESC LIMIT 10");
        //dd($countrys);
        return view('home',
            compact('lastTens','openHighs','delayeds','countrys'));
    }
}

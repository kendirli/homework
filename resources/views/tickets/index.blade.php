@extends('layouts.master')
@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('back/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('back/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tickets</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Durumu</label>
                                            <select class="form-control select2" name="status" id="status" style="width: 75%;"
                                                    onchange="changeStatus()">
                                                <option value="3" @if($status == 3) selected @endif>Tümü</option>
                                                <option value="1" @if($status == 1) selected @endif>Açık</option>
                                                <option value="0" @if($status == 0) selected @endif>Kapalı</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <a href="/tickets/create" class="btn btn-success"> Add Ticket</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>Country</th>
                                        <th>Priority</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td>{{$ticket->user->name}}</td>
                                            <td>{{$ticket->title}}</td>
                                            <td>{{$ticket->content}}</td>
                                            <td>{{$ticket->country->name}}</td>
                                            <td>
                                                @if($ticket->priority == 1)
                                                    Yüksek
                                                @elseif($ticket->priority == 2)
                                                    Orta
                                                @else
                                                    Düşük
                                                @endif
                                            </td>
                                            <td>
                                                @if($ticket->status == 1)
                                                    <span class="text-success text-bold">Açık</span>
                                                @else
                                                    <span class="text-danger text-bold">Kapalı</span>
                                                @endif
                                            </td>
                                            <td>{{$ticket->created_at->locale('tr')->diffForHumans('')}}</td>
                                            <td>
                                                @if($ticket->status == 1)
                                                    <a class="btn btn-danger tamamlandi" id="{{$ticket->id}}">Tamamlandı</a>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/tickets/edit/{{$ticket->id}}" class="btn btn-primary">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="/tickets/delete/{{$ticket->id}}" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
    </div>
@endsection
@section('java-script')
    <script src="{{asset('back/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('back/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('back/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('back/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('back/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false
            });
            $(".tamamlandi").click(function (){
                var id = this.id;
                $.get({
                    type:"GET",
                    url:"/tickets/close/"+id,
                    success:function (result){
                        if (result == 1){
                            location.reload();
                        }else{
                            console.log(result)
                        }
                    }
                });
            });
        });
        function changeStatus(){
            var status = document.getElementById('status')
            window.location.href = "/tickets/" + status.value;
        }

    </script>
@endsection
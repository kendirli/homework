<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('tr_TR');
        $limit = 5;
        $password = ['123456789','asdqwezxc','asd123456','sefa123456','12345asd'];
        for($i=0;$i<$limit;$i++){
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->unique()->email;
            $user->password = Hash::make($password[$i]);
            $user->save();
        }
    }
}

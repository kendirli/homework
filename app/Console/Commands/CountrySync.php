<?php

namespace App\Console\Commands;

use App\Country;
use Illuminate\Console\Command;

class CountrySync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'country:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ülkeleri Eşitleme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $json = file_get_contents("https://api.first.org/data/v1/countries");
        $getCountry = json_decode($json,true);
        foreach ($getCountry['data'] as $key => $item){
            //dd($key);
            $country = $item['country'];
            $region = $item['region'];
            $query = Country::where('code',$key)->first();
            if(!$query){
                $addCountry = new Country();
                $addCountry->code = $key;
                $addCountry->name = $country;
                $addCountry->region = $region;
                $addCountry->save();
            }
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    protected $table = 'country';
    protected $dates = [
        'created_at','updated_at','deleted_at'
    ];
    protected $fillable = [
        'code','name','region'
    ];

    public function ticket()
    {
        return $this->hasMany('App\Ticket','country_id','id');
    }
}

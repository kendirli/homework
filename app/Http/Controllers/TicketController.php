<?php

namespace App\Http\Controllers;

use App\Country;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $tickets = Ticket::all()->sortByDesc('id');
            $status = "3";
        //dd($status);
        return view('tickets.index',compact('tickets','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys = Country::all();
        return view('tickets.create',compact('countrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = \Auth::user()->id;
        $data['status']  = 1;
        //dd($data);
        try {
            Ticket::create($data);
            return redirect()->back();
        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    public function indexChangeStatus($status)
    {
        if($status == 3){
            return redirect('/tickets');
        }else{
            $tickets = Ticket::where('status',$status)->get();
        }
        //dd($status);
        return view('tickets.index',compact('tickets','status'));
    }

    public function closeTicket($id)
    {
        try {
            Ticket::where('id',$id)->update(['status'=>0]);
            return 1;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);
        if($ticket->status == 0){
            return redirect('/tickets');
        }
        $countrys = Country::all();
        return view('tickets.edit',compact('ticket','countrys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            //dd($data);
            Ticket::where('id',$id)->update($data);
            return redirect()->back();
        }catch (\Exception $e){
            return redirect('/tickets');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Ticket::where('id',$id)->delete();
            return redirect()->back();
        }catch (\Exception $e){
            return redirect('/tickets');
        }
    }
}

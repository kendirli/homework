<?php

use Illuminate\Database\Seeder;
use App\Ticket;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('tr_TR');

        for($i=0; $i<100; $i++) {
            Ticket::create([
                'user_id' => rand(1,5),
                'country_id' => rand(1,100),
                'title' => $faker->sentence(),
                'content' => $faker->text(100),
                'priority' => $faker->randomElement($array = array('1','2','3')),
                'status' => $faker->randomElement($array = array('0','1')),
                'created_at' => $faker->dateTimeBetween('-1 years')
            ]);
        }
    }
}
